const express = require('express');
const router = express.Router();
const passport = require('passport');
const Token = require('../token/token');
const fs = require('fs');
const constant = require('./../constants/constants');
const Restaurant = require('../models/restaurant');


// {ok: , accessToken: , errorMensaje: , error: }
// { error: result: errorMensaje}
const random = constant.random;
router.get("",  passport.authenticate('jwt', { session: false }),
    (req, res) => {

        Restaurant.getAllRestaurants(req.user.id).then(result => {
            res.status(200).send({ result: result, 
                accessToken: Token.createToken(req.user.id) });
        }).catch(error => {
            let mensaje = error == null ? 'No hay restaurantes ' : 'Error al buscar los restarantes ';
            res.status(404).send({error: true, 
                errorMensaje: mensaje + error});
        })
    }
);

router.get("/mine", passport.authenticate('jwt', { session: false }),
    (req, res) => {

        Restaurant.getMyRestaurants(req.user.id).then(result => {
            res.status(200).send({ result: result, 
                accessToken: Token.createToken(req.user.id) });
        }).catch(error => {
            let mensaje = error == null ? 'El usuario no tiene restaurantes ' : 'Error al buscar los restarantes del usuario ';
            res.status(400).send({ error: true, 
                errorMensaje: mensaje +error});
        })
    });

router.get("/user/:id", passport.authenticate('jwt', { session: false }),
    (req, res) => {

        Restaurant.getUserRestaurants(req.params.id, req.user.id).then(result => {
            res.status(200).send({ result: result,
                 accessToken: Token.createToken(req.user.id) });
        }).catch(error => {
            let mensaje = error == null ? 'El usuario no tiene restaurantes ' : 'Error al buscar los restarantes del usuario ';
            res.status(404).send({ error: true, 
                errorMensaje: mensaje +error });
        })
    });

router.get("/:id",  passport.authenticate('jwt', { session: false }),
(req, res) => {

    Restaurant.getRestaurantId(req.params.id, req.user.id).then(result => {
        res.status(200).send({ result: result, 
            accessToken: Token.createToken(req.user.id) });
    }).catch(error => {
        let mensaje = error == null ? 'Restaurante no encontrado ' : 'Error al buscar el restarante  ';
        res.status(404).send({error: true, 
            errorMensaje: mensaje + error});
    })
});

router.post("", passport.authenticate('jwt', { session: false }),
(req, res) => {
    let restaurante = new Restaurant(req.body);
    restaurante.image = './images/restaurants/' + random + '.jpg';

    restaurante.insertRestaurant(req.user.id).then(async result => {
        let archivo = Buffer.from(req.body.image, 'base64');
        await fs.writeFile('./images/restaurants/' + random + '.jpg', archivo, 'base64', (err) => {
        if (err) throw err;
            console.log('Archivo subido correctamente');
        });

        res.status(200).send({ result: result, 
            accessToken: Token.createToken(req.user.id) });
    }).catch(error => {
        let mensaje = error == null ? 'Restaurante no insertado ' : 'Error al insertar el restarante  ';
        res.status(400).send({error: true, errorMensaje: mensaje + error});
    })
});

router.put("/:id", passport.authenticate('jwt', { session: false }),
(req, res) => {
    let restaurante = new Restaurant(req.body);
    let restId = req.params.id;
    restaurante.image = './images/restaurants/' + random + '.jpg';

    restaurante.updateRestaurant(restId, req.user.id).then(async result => {
        let archivo = Buffer.from(req.body.image, 'base64');
        await fs.writeFile('./images/restaurants/' + random + '.jpg', archivo, 'base64', (err) => {
        if (err) throw err;
            console.log('Archivo subido correctamente');
        });

        res.status(200).send({ result: result, 
            accessToken: Token.createToken(req.user.id) });
    }).catch(error => {
        let mensaje = error == null ? 'NO eres propiertario ' : 'Error '
        res.status(404).send({error: true, errorMensaje: mensaje});
    })
});

router.delete("/:id", passport.authenticate('jwt', { session: false }),
(req, res) => {
    let restId = req.params.id;

    Restaurant.deleteRestaurant(restId, req.user.id).then(result => {
        res.status(200).send({ result: restId, accessToken: Token.createToken(req.user.id) });
    }).catch(error => {
        let mensaje = error == null ? 'NO eres propiertario ' : 'Error '
        res.status(404).send({error: true, errorMensaje: mensaje + error});
    })
});

router.get("/:id/comments", passport.authenticate('jwt', { session: false }),
(req, res) => {
    let restId = req.params.id;

    Restaurant.getRestaurantComments(restId).then(result => {
        let comentarios = [];
        result.forEach(com => {
            let coment = {
                id: com.id,
                stars: com.stars,
                restaurantId: com.restaurantId,
                userId: com.userId,
                text: com.text,
                date: com.date,
                user: { name: com.name, avatar: com.avatar, email: com.email, lat: com.lat, lng: com.lng  }
            };
            comentarios.push(coment);
        })

        res.status(200).send({ result: comentarios, accessToken: Token.createToken(req.user.id) });
    }).catch(error => {
        let mensaje = error == null ? 'Comentarios no encontrados ' : 'Error al buscar los comentarios  ';
        res.status(404).send({error: true, errorMensaje: mensaje + error });
    })
});


module.exports = router;