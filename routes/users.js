const express = require('express');
const router = express.Router();
const Token = require('../token/token');
const passport = require('passport');
const fs = require('fs');
const constant = require('./../constants/constants');
const User = require('../models/user');


// {ok: , accessToken: , errorMensaje: , error:  result}
const random = constant.random;
router.get("/me", passport.authenticate('jwt', { session: false }),
(req, res) => {
    User.getUser(req.user.id).then(result => {
        res.status(200).send({user: result, 
            token: Token.createToken(req.user.id)});
    }).catch(error => {
        let mensaje = error == null ? 'Usuario no encontrado ' : 'Error al buscar usuario ';
        res.status(400).send({error: true, errorMensaje: mensaje + error });
    });
});

router.get("/:id", passport.authenticate('jwt', { session: false }),
(req, res) => {
    let userId = req.params.id;
    User.getUser(userId).then(result => {
        res.status(200).send({user: result, 
            token: Token.createToken(req.user.id)});
    }).catch(error => {
        let mensaje = error == null ? 'Usuario no encontrado ' : 'Error al buscar usuario ';
        res.status(404).send({error: true, errorMensaje: mensaje +error });
    });
});

router.put("/me", passport.authenticate('jwt', { session: false }),
(req, res) => {
    User.getUser(req.user.id).then(resp => {
        let usuario = new User(resp);
        usuario.email = req.body.email;
        usuario.name = req.body.name;
        
        usuario.updateUserInfo(req.user.id).then(result => {
            res.status(200).send({ok: true, 
                token: Token.createToken(req.user.id)});
        }).catch(error => {
            let mensaje = error == null ? 'Usuario no encontrado ' : 'Error al editar usuario ';
            res.status(400).send({error: true, errorMensaje: error});
        });
    }).catch(error => {
        let mensaje = error == null ? 'Usuario no encontrado ' : 'Error al buscar usuario ';
        res.status(400).send({error: true, errorMensaje: mensaje +error});
    });
});

router.put("/me/avatar", passport.authenticate('jwt', { session: false }),
(req, res) => {
    User.getUser(req.user.id).then(resp => {
        let usuario = new User(resp);
        usuario.avatar = './images/users/' + random + '.jpg';

        usuario.updateUserInfo(req.user.id).then(result => {
            let archivo = Buffer.from(req.body.avatar, 'base64');
            fs.writeFile('./images/users/' + random + '.jpg', archivo, 'base64', (err) => {
            if (err) throw err;
                console.log('Imagen subida correctamente');
            });
            res.status(200).send({avatar: '/images/users/' + random + '.jpg', 
            token: Token.createToken(req.user.id)});
        }).catch(error => {
            let mensaje = error == null ? 'Usuario no encontrado ' : 'Error al editar usuario ';
            res.status(400).send({error: true, errorMensaje: mensaje +error });
        });
    }).catch(error => {
        let mensaje = error == null ? 'Usuario no encontrado ' : 'Error al buscar usuario ';
        res.status(400).send({error: true, errorMensaje: mensaje + error});
    })
});

router.put("/me/password",  passport.authenticate('jwt', { session: false }),
(req, res) => {
    User.getUser(req.user.id).then(resp => {
        let usuario = new User(resp);
        usuario.password = req.body.password;

        usuario.updateUserInfo(req.user.id)
        .then(result => {
            res.status(200).send({ok: true, 
                token: Token.createToken(req.user.id)});
        })
        .catch(error => {
            let mensaje = error == null ? 'Usuario no encontrado ' : 'Error al editar usuario ';
            res.status(400).send({error: true, errorMensaje: mensaje + error});
        });
    }).catch(error => {
        let mensaje = error == null ? 'Usuario no encontrado ' : 'Error al buscar usuario ';
        res.status(400).send({error: true, errorMensaje: mensaje + error});
    });
});


module.exports = router;