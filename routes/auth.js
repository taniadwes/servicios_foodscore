const express = require("express");
const router = express.Router();
const User = require("./../models/user");
const constant = require('./../constants/constants');
const passport = require("passport");
const fs = require("fs");


// {ok: , accessToken: , errorMensaje: , error: }
let random = constant.random;
router.post("/register", (req, res) => {

    let user = new User(req.body);
    user.avatar = './images/users/' + random + '.jpg';

    user.newUser().then(result => {
        let file = Buffer.from(req.body.avatar, 'base64');
        fs.writeFile('./images/users/' + random +'.jpg' , file, 'base64', (err) => {
            if(err) throw err;
            console.log('Archivo avatar subido correctamente');
        });
        res.send(result);
    }).catch( error => {
        res.status(400).send({errorMensaje: "Error en e registro", error: error});
    });
});


router.post("/login", (req, res) =>{
    User.validarUser({ email: req.body.email, password: req.body.password})
    .then( result => {
        if (result) res.send( {ok: true, accessToken: result});
        else res.status(404).send({ok: false, errorMensaje: "Usuario no válido"});
    })
    .catch( error => {
        res.status(404).send({ok: false, errorMensaje: "Error al loguear", error: error});
    });
});


router.get("/validate", passport.authenticate("jwt", { session: false }), 
(req, res) =>{
    if( req.user) {
        res.status(200).send({ok: true});
    }else {
        req.status(401).send("Unauthorized");  // 401 403 forbidden
    }
});

module.exports = router;