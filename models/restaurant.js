const conexion = require("../bd/bdconfig");

module.exports = class Restaurant {
  constructor(restJSON) {
    this.id = restJSON.id;
    this.name = restJSON.name;
    this.description = restJSON.description;
    this.daysOpen = restJSON.daysOpen;
    this.phone = restJSON.phone;
    this.image = restJSON.image;
    this.cuisine = restJSON.cuisine;
    this.creatorId = restJSON.creatorId;
    this.stars = restJSON.stars || 0;
    this.lat = restJSON.lat || 0;
    this.lng = restJSON.lng || 0;
    this.address = restJSON.address;
    this.distance = restJSON.distance;
    this.mine = restJSON.mine;
  }

  static getAllRestaurants(userId) {
    return new Promise((resolve, reject) => {
      conexion.query(
        "SELECT r.id, r.name, r.description, r.daysOpen, r.phone, r.image, r.cuisine, r.creatorId, r.stars, r.lat, r.lng, r.address, haversine(user.lat, user.lng, r.lat, r.lng) as distance FROM user, restaurant r WHERE user.id = ?",
        [userId],
        (error, resultado, campos) => {
          if (error) return reject(error);
          else
            return resolve(
              resultado.map(rest => {
                rest.cuisine = rest.cuisine.split(", ");
                rest.daysOpen = rest.daysOpen.split(", ");

                if (rest.creatorId == userId) rest.mine = true;
                else rest.mine = false;

                return new Restaurant(rest);
              })
            );
        }
      );
    });
  }

  static getMyRestaurants(userId) {
    return new Promise((resolve, reject) => {
      conexion.query(
        "SELECT r.id, r.name, r.description, r.daysOpen, r.phone, r.image, r.cuisine, r.creatorId, r.stars, r.lat, r.lng, r.address, haversine(user.lat, user.lng, r.lat, r.lng) as distance FROM user, restaurant r WHERE user.id = ? AND r.creatorId = ?",
        [userId, userId],
        (error, resultado, campos) => {
          if (error) return reject(error);
          else
            return resolve(
              resultado.map(rest => {
                rest.cuisine = rest.cuisine.split(", ");
                rest.daysOpen = rest.daysOpen.split(", ");

                if (rest.creatorId == userId) rest.mine = true;
                else rest.mine = false;

                return new Restaurant(rest);
              })
            );
        }
      );
    });
  }

  static getUserRestaurants(userId, id) {
    return new Promise((resolve, reject) => {
      conexion.query(
        "SELECT r.id, r.name, r.description, r.daysOpen, r.phone, r.image, r.cuisine, r.creatorId, r.stars, r.lat, r.lng, r.address, haversine(user.lat, user.lng, r.lat, r.lng) as distance FROM user, restaurant r WHERE user.id = ? AND r.creatorId = ?",
        [id, userId],
        (error, resultado, campos) => {
          resultado;
          if (error || resultado.length == 0) return reject(error);
          else
            return resolve(
              resultado.map(rest => {
                if (rest.creatorId == userId) rest.mine = true;
                else rest.mine = false;

                rest.cuisine = rest.cuisine.split(",");
                rest.daysOpen = rest.daysOpen.split(",");
                return new Restaurant(rest);
              })
            );
        }
      );
    });
  }

  static getRestaurantId(restId, userId) {
    return new Promise((resolve, reject) => {
      conexion.query(
        "SELECT r.id, r.name, r.description, r.daysOpen, r.phone, r.image, r.cuisine, r.creatorId, r.stars, r.lat, r.lng, r.address, haversine(user.lat, user.lng, r.lat, r.lng) as distance FROM user, restaurant r WHERE user.id = ? AND r.id = ?",
        [userId, restId],
        (error, resultado, campos) => {
          if (error || resultado.length == 0) return reject(error);
          else
            return resolve(
              resultado.map(rest => {
                if (rest.creatorId == userId) rest.mine = true;
                else rest.mine = false;

                rest.cuisine = rest.cuisine.split(",");
                rest.daysOpen = rest.daysOpen.split(",");
                return new Restaurant(rest);
              })
            );
        }
      );
    });
  }

  static deleteRestaurant(restId, id) {
    return new Promise((resolve, reject) => {
      conexion.query(
        "DELETE FROM restaurant WHERE id = ? AND creatorId = ? ",
        [restId, id],
        (error, resultado, campos) => {
          if (error || resultado.affectedRows == 0) return reject(error);
          else return resolve(resultado);
        }
      );
    });
  }

  static getRestaurantComments(restId) {
    return new Promise((resolve, reject) => {
      conexion.query(
        "SELECT comment.*, user.* FROM comment, user WHERE comment.restaurantId = ? AND user.id = comment.userId",
        [restId],
        (error, resultado, campos) => {
          if (error) return reject(error);
          else return resolve(resultado);
        }
      );
    });
  }

  static addComment(comment) {
    return new Promise((resolve, reject) => {
      conexion.query(
        "INSERT INTO comment SET ? ",
        [comment],
        (error, resultado, campos) => {

          if (error || resultado.affectedRows == 0) return reject(error);
          else return resolve(resultado);
        }
      );
    });
  }

  static getCommentUser(id, userId) {
    return new Promise((resolve, reject) => {
      conexion.query(
        "SELECT comment.*, user.* FROM comment, user WHERE comment.ID = ? AND user.ID = ? ",
        [id, userId],
        (error, resultado, campos) => {
          if (error) return reject(error);
          else return resolve(resultado[0]);
        }
      );
    });
  }

  insertRestaurant(id) {
    return new Promise((resolve, reject) => {
      let datos = {
        name: this.name,
        description: this.description,
        daysOpen: this.daysOpen.join(","),
        phone: this.phone,
        image: this.image,
        cuisine: this.cuisine.join(","),
        creatorId: id,
        stars: this.stars,
        lat: this.lat,
        lng: this.lng,
        address: this.address
      };
      conexion.query(
        "INSERT INTO restaurant SET ? ",
        [datos],
        (error, resultado, campos) => {
          if (error || resultado.affectedRows == 0) return reject(error);
          else {
            datos.cuisine = datos.cuisine.split(",");
            datos.daysOpen = datos.daysOpen.split(",");

            return resolve(datos);
          }
        }
      );
    });
  }


  updateRestaurant(restId, id) {
    return new Promise((resolve, reject) => {

      let datos = {
        name: this.name,
        description: this.description,
        daysOpen: this.daysOpen.join(","),
        phone: this.phone,
        image: this.image,
        cuisine: this.cuisine.join(","),
        creatorId: id,
        stars: this.stars,
        lat: this.lat,
        lng: this.lng,
        address: this.address
      };
      conexion.query(
        "UPDATE restaurant SET ? WHERE id = ? AND creatorId = ? ",
        [datos, restId, id],
        (error, resultado, campos) => {
          if (error || resultado.affectedRows == 0) return reject(error);
          else {
            datos.cuisine = datos.cuisine.split(",");
            datos.daysOpen = datos.daysOpen.split(",");

            return resolve(datos);
          }
        }
      );
    });
  }



};
