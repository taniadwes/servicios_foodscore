const conexion = require("../bd/bdconfig");
const Token = require("../token/token");
const md5 = require("md5");

module.exports = class User {
  constructor(userJSON) {
    this.id = userJSON.is || -1;
    this.name = userJSON.name || "";
    this.password = userJSON.password || "";
    this.avatar = userJSON.avatar || "";
    this.email = userJSON.email || "";
    this.lat = userJSON.lat || 0;
    this.lng = userJSON.lng || 0;
  }

  newUser() {
    return new Promise((resolve, reject) => {
      let passEncrypted = md5(this.password + "");
      let datos = {
        name: this.name,
        email: this.email,
        password: passEncrypted,
        lat: this.lat,
        lng: this.lng,
        avatar: this.avatar
      };

      conexion.query(
        "INSERT INTO user SET ?",
        datos,
        (error, resultado, campos) => {
          if (error || resultado.affectedRows == 0) return reject(error);
          else return resolve(resultado);
        }
      );
    });
  }

  static getUser(id) {
    return new Promise((resolve, reject) => {
      conexion.query(
        "SELECT * FROM user WHERE id = ?",
        [id],
        (error, resultado, campos) => {
          if (error || resultado.length == 0) return reject(error);
          else return resolve(resultado[0]);
        }
      );
    });
  }

  updateUserInfo(id) {
    return new Promise((resolve, reject) => {
      let passEncrypted = md5(this.password + "");
      let datos = {
        name: this.name,
        email: this.email,
        password: passEncrypted,
        lat: this.lat,
        lng: this.lng,
        avatar: this.avatar
      };

      conexion.query(
        "UPDATE user SET ? WHERE id = ?",
        [datos, id],
        (error, resultado, campos) => {
          if (error || resultado.affectedRows == 0) return reject(error);
          else return resolve(resultado);
        }
      );
    });
  }

  static validarUser(userJSON) {
    return new Promise((resolve, reject) => {
      let passEncrypted = md5(userJSON.password + "");
      conexion.query(
        "SELECT id, email, password FROM user WHERE email = ? AND password = ?",
        [userJSON.email, passEncrypted],
        (error, resultado, campos) => {
          if (error || resultado.length == 0) return reject(error);
          else return resolve(Token.createToken(resultado[0].id));
        }
      );
    });
  }

};
