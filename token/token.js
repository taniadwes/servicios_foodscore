const jwt = require("jsonwebtoken");
const constant = require("../constants/constants");
const secreto = constant.secreto;

module.exports = class Token {

  static createToken(id) {
    return jwt.sign({ id: id }, secreto, { expiresIn: "2 hours" });
  }

  static validarToken(token) {
    try {
      let resultado = jwt.verify(token, secreto);
      return resultado;
    } catch (e) {}
  }
  
};
