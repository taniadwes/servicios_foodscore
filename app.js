const express = require("express");
const bodyParser = require("body-parser");
const passport = require("passport");
const { Strategy, ExtractJwt } = require("passport-jwt");

const auth = require("./routes/auth");
const restaurants = require("./routes/restaurants");
const users = require("./routes/users");
const constant = require('./constants/constants');

const secreto = constant.secreto;

let app = express();
app.use(bodyParser.json());
app.use(passport.initialize());

passport.use(
  new Strategy(
    {
      secretOrKey: secreto,
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
    },
    (payload, done) => {
      if (payload.id) {
        return done(null, { id: payload.id });
      } else {
        return done(new Error("Usuario incorrecto"), null);
      }
    }
  )
);

app.use("/auth", auth);
app.use("/users", users);
app.use("/restaurants", restaurants);



console.log(new Date().getTime());

app.listen(8080);
